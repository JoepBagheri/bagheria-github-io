---
layout: page
title: Publications
---

I can also be found on [Google Scholar](https://scholar.google.nl/citations?user=QWhiQdgAAAAJ&hl=en&oi=ao).

## 2019

<img src="../img/JOURNAL.png" height="20px">
Arjan Sammani, Mark Jansen, M. Linschoten, Ayoub Bagheri, N. de Jonge, H. Kirkels, L. van Laake, Aryan Vink, J.P van Tintelen, D. Dooijes, A.S.J.M. te Riele, M. Harakalova, A.F. Baas, and Folkert W. Asselbergs. *UNRAVEL: Big data analytics Research Data Platform to improve care of patients with cardiomyopathies using routine electronic health records and standardized Biobanking*, Netherlands Heart Journal, Springer, pp.1-9. [Link](https://link.springer.com/article/10.1007/s12471-019-1288-4)


## 2018

<img src="../img/JOURNAL.png" height="20px">
Ayoub Bagheri. *Integrating word status for joint detection of sentiment and aspect in reviews*, SAGE, Journal of Information Science. [JIS](https://journals.sagepub.com/doi/full/10.1177/0165551518811458)

<img src="../img/JOURNAL.png" height="20px">
Razieh Asgarnezhad, S Amirhassan Monadjemi, Mohammadreza Soltanaghaei and Ayoub Bagheri. *SFT: A model for sentiment classification using supervised methods on twitter*, [Journal of Theoretical and Applied Information Technology](https://www.jatit.org/), 2018. [[PDF &#8594;]](https://bagheria.github.io/publications/sft.pdf)

<img src="../img/CONFERENCE.png" height="20px">
Mahsa Afsharizadeh, Hossein Ebrahimpour-Komleh and Ayoub Bagheri. *Query-oriented text summarization using sentence extraction technique*, accepted at 4th IEEE International Conference on Web Research [ICWR 2018](http://iranwebconf.ir/). [[IEEEXplore &#8594;]](https://ieeexplore.ieee.org/abstract/document/8387248/)


## 2017

<img src="../img/CONFERENCE.png" height="20px">
Atieh Jabalameli, S. Mehdi Vahidipour and Ayoub Bagheri. *A centralized crawler for web community detection*, accepted at 4th International Conference on Knowledge-Based Engineering and Innovation [KBEI 2017](http://kbei.ir/en/).

<img src="../img/CONFERENCE.png" height="20px">
Zeinab Sedighi, Hossein Ebrahimpour-Komleh and Ayoub Bagheri. *RLOSD: Representation Learning Based on OpinionSpam Detection*, accepted at 11th International Conference on Computer Science and Information Technologies, 2017. [[IEEEXplore &#8594;]](https://ieeexplore.ieee.org/abstract/document/8311593)

<img src="../img/CONFERENCE.png" height="20px">
Elham Madjidi and Ayoub Bagheri. *Providing a method for classifying users' comments using machine learning and voting algorithm*, accepted at 1st International conference on Modern Technologies in Sciences [MTS 2017](http://mtsconf.ir/index.aspx).

<img src="../img/CONFERENCE.png" height="20px">
Mohamad Javad Maleki and Ayoub Bagheri. *Multi-word effect on analysis of online reviews sentiments*, accepted at 3rd International Conference on Pattern Recognition & Image Analysis [IPRIA 2017](https://ieeexplore.ieee.org/xpl/mostRecentIssue.jsp?punumber=7976174).


## 2016

<img src="../img/JOURNAL.png" height="20px">
Ali Zaghian and Ayoub Bagheri. *A combined model of clustering and classification methods for preserving privacy in social networks against inference and neighborhood attacks*, [International Journal of Security and Its Applications](http://www.sersc.org/journals/IJSIA/), 2016. [[PDF &#8594;]](https://bagheria.github.io/publications/Privacy%20paper.pdf)


## 2015

<img src="../img/JOURNAL.png" height="20px">
Ayoub Bagheri, Mohamad Saraee and Shiva Nadi. *PSA: a hybrid feature selection approach for Persian text classification*, [Journal of Computing and Security](http://jcomsec.ui.ac.ir/), 2015. [[PDF &#8594;]](https://bagheria.github.io/publications/PSA.pdf)


## 2014

<img src="../img/JOURNAL.png" height="20px">
Ayoub Bagheri and Mohamad Saraee. *Persian Sentiment Analyzer: A Framework based on a Novel Feature Selection Method*, [International Journal of Artificial Intelligence](http://www.ceser.in/ceserp/index.php/ijai), 2014. [[PDF &#8594;]](https://bagheria.github.io/publications/IJAI_6.pdf)

<img src="../img/JOURNAL.png" height="20px">
Ayoub Bagheri, Mohamad Saraee and Franciska De Jong. *ADM-LDA: An aspect detection model based on topic modelling using the structure of review sentences*, [Journal of Information Science](http://journals.sagepub.com/home/jis), 2014. [[PDF &#8594;]](http://journals.sagepub.com/doi/pdf/10.1177/0165551514538744)


## 2013

<img src="../img/JOURNAL.png" height="20px">
Ayoub Bagheri, Mohamad Saraee and Franciska De Jong. *Care more about customers: unsupervised domain-independent aspect detection for sentiment analysis of customer reviews*, [Knowledge-Based Systems](https://www.journals.elsevier.com/knowledge-based-systems), 2013. [[PDF &#8594;]](https://www.sciencedirect.com/science/article/pii/S0950705113002414)

<img src="../img/CONFERENCE.png" height="20px">
Ayoub Bagheri, Mohamad Saraee and Franciska De Jong. *Latent Dirichlet Markov allocation for sentiment analysis*, accepted at the fifth European Conference on Intelligent Management Systems in Operations [IMSIO 5](http://www.theorsociety.com/Pages/Conferences/IMSIO5/IMSIO5.aspx)[[PDF &#8594;]](http://usir.salford.ac.uk/29460/1/Latent_Dirichlet_Markov_Allocation.pdf).

<img src="../img/CONFERENCE.png" height="20px">
Ayoub Bagheri, Mohamad Saraee and Franciska De Jong. *An unsupervised aspect detection model for sentiment analysis of reviews*, accepted at 18th International Conference on Application of Natural Language to Information Systems [NLDB 2013](http://nldb.csesalford.com/) [[PDF &#8594;]](https://bagheria.github.io/publications/NLDB1.pdf).

<img src="../img/CONFERENCE.png" height="20px">
Mohamad Saraee and Ayoub Bagheri. *Feature selection methods in Persian sentiment analysis*, accepted at 18th International Conference on Application of Natural Language to Information Systems [NLDB 2013](http://nldb.csesalford.com/)[[PDF &#8594;]](http://usir.salford.ac.uk/29459/1/nldb2013_submission_44.pdf).

<img src="../img/CONFERENCE.png" height="20px">
Ayoub Bagheri, Mohamad Saraee and Franciska De Jong. *Sentiment classification in Persian: Introducing a mutual information-based method for feature selection*, accepted at 21st Iranian Conference on Electrical Engineering [ICEE 2013](https://ieeexplore.ieee.org/document/6599671).


## 2011

<img src="../img/CONFERENCE.png" height="20px">
Mohamad Saraee, Mehdi Moghimi and Ayoub Bagheri. *Modeling batch annealing process using data mining techniques for cold rolled steel sheets*, accepted at the First International Workshop on Data Mining for Service and Maintenance [KDD 2011](https://dl.acm.org/citation.cfm?id=2018673) [[PDF &#8594;]](https://bagheria.github.io/publications/ACM_Saraee_Annealing.pdf).

<img src="../img/JOURNAL.png" height="20px">
Shiva Nadi, Mohammad Saraee and Ayoub Bagheri. *A hybrid recommender system for dynamic web users*, [International Journal Multimedia and Image Processing](http://infonomics-society.org/ijmip/), 2011. [[PDF &#8594;]](https://bagheria.github.io/publications/HRS.pdf).

<img src="../img/JOURNAL.png" height="20px">
Shiva Nadi, Mohammad Saraee, Ayoub Bagheri and Mohamad Davarpanah Jazi. *FARS: Fuzzy ant based recommender system for web users*, [International Journal of Computer Science Issues](http://www.ijcsi.org/), 2011. [[PDF &#8594;]](https://bagheria.github.io/publications/IJCSI-8-1-203-209_Nadi_1.pdf).


## 2010

<img src="../img/CONFERENCE.png" height="20px">
Shiva Nadi, Mohamad Saraee, Mohamad Davarpanah Jazi and Ayoub Bagheri. *A Hybrid Recommender System Based on Collaborative Behavior of Ants*, accepted at the Fourth Iran Data Mining Conference, 2010.

<img src="../img/CONFERENCE.png" height="20px">
Mohamad Saraee, Mehdi Moghimi and Ayoub Bagheri. *Persian Text Classification: Presenting a New Method by Combining SVM and TFV algorithms*, accepted at the 7th International Industrial Engineering Conference, 2010.


## 2009

<img src="../img/CONFERENCE.png" height="20px">
Mohammad S Norouzzadeh, Ayoub Bagheri and Mohammad  Saraee. *Web search personalization: A fuzzy adaptive approach*, accepted at 2nd IEEE International Conference on Computer Science and Information Technology, [ICCSIT 2009] (https://ieeexplore.ieee.org/abstract/document/5234590) [[PDF &#8594;]](https://bagheria.github.io/publications/norouzzadeh2009.pdf).


## 2008

<img src="../img/CONFERENCE.png" height="20px">
Ayoub Bagheri, Hamed Farzanehfar, Mohammad Saraee and Mohammad R. Ahmadzadeh. *Text news classification with Naïve Bayes algorithm*, accepted at the 2nd Iran Data Mining Conference, 2008.

<img src="../img/CONFERENCE.png" height="20px">
Ayoub Bagheri, Maziar Palhang and Mohammad R. Ahmadzadeh. *Defeating CAPTCHAs with Learning Algorithms*, accepted at the International ISC Conference on Information Security and Cryptology, 2008.

<img src="../img/JOURNAL.png" height="20px">
Ayoub Bagheri, Mohammad-R Akbarzadeh-T and Mohamad Saraee. *Finding shortest path with learning algorithms*, [International Journal of Artificial Intelligence](http://www.ceser.in/ceserp/index.php/ijai), 2008. [[PDF &#8594;]](https://bagheria.github.io/publications/Finding_Shortest_Path_with_Learning_Algo.pdf)
