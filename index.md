---
layout: page
title: Ayoub Bagheri
subtitle: <H4> DATA SCIENTIST, Utrecht University </H4>
email: a.bagheri@uu.nl
---

### About me

I am a researcher at the Department of Methodology and Statistics in the [Utrecht University](https://www.uu.nl/en) and also affiliated with the Department of Cardiology, Division of Heart and Lungs, [University Medical Center Utrecht (UMCU)](https://www.umcutrecht.nl/en/1).

Currently, I am working on Big Data Analysis for Cardiovascular Diseases and how to incorporate clinical text data and clinical trial data into the learning process at population data.

##### Download my curriculum vitae from [here.](https://bagheria.github.io/publications/CV - Ayoub Bagheri - July 2019.pdf)

### Research Interests

1. Text Mining
2. Natural Language Processing
3. Machine Learning
4. Medical Data Mining
5. Deep Learning

### Projects
- A data mining-based workbench for UK Biobank: advancing precision medicine by the use of machine learning and expert knowledge

- Machine learning techniques to predict worsening of diastolic dysfunction in patients from an outpatient clinic

- Deep neural networks for icd-10 classification of diagnosis registration in cardiovascular notes to allow data mining in electronic health records

- Natural language processing for EHR precision medicine

- CONVOCALS: a CONVOlutional neural network to predict symptoms and major secondary CArdiovascuLar events based on high-resolution scanned histological Slides 

### Organizational duties and activities
- [Statistical Programming with R 2019, Summer School Utrecht University](https://www.gerkovink.com/R/)

- [Big Data in Health Research 2019, Summer School UMC Utrecht University](https://utrechtsummerschool.nl/courses/life-sciences/big-data-in-health-research)
 
- Invited talk ([slides](https://drive.google.com/file/d/18ZTa0fdJhzxxeX8zpyVG7aAX90V9iAJl/view?usp=sharing)) at the *[Kick-off meeting Special Interest Group Text Mining](https://www.uu.nl/en/events/kick-off-meeting-special-interest-group-text-mining), Utrecht University* (August 2018)

- Invited talk on __NLP for healthcare__ at the _Big Data in Health Research, Utrecht summer school, University Medical Center Utrecht_ (August 2018)

### Contact

```
Utrecht University - M&S
3584 CH  UTRECHT
The Netherlands

Office: Sjoerd Groenmangebouw, Padualaan 14, C1.22
Email: a.bagheri[at]uu.nl
       a.bagheri-2[at]umcutrecht.nl
       ayoub.bagheri[at]gmail.com
Phone number: +31 30 253 5857
```
